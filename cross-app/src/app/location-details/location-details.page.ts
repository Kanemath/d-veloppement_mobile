import { Component, OnInit } from '@angular/core';
import { LocationsService } from '../services/locations.service';
import { Place } from '../model/place.model';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { AlertController } from '@ionic/angular';



@Component({
  selector: 'app-location-details',
  templateUrl: './location-details.page.html',
  styleUrls: ['./location-details.page.scss'],
})
export class LocationDetailsPage implements OnInit {

  private currentPlace: Place;
  constructor(private locService: LocationsService, private camera: Camera, private alertCtrlr: AlertController) { }

  ngOnInit() {
    this.currentPlace = this.locService.currentLocation;
  }
  public async onTakePicture() {
    const option1: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.CAMERA,
      allowEdit: true,
    };
    const option2: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit: true
    };

    let alert = await this.alertCtrlr.create({
        header: 'Confirmation',
        message: 'Source?',
        buttons: [
          {
            text: 'Camera',
              handler: () => {
                this.getPicture(option1);
              }
          },
          {
            text: 'Library',
              handler: () => {
                this.getPicture(option2);
              }
          }
        ]
    });
    await alert.present();
  }
  getPicture(option: CameraOptions) {
    this.camera.getPicture(option).then(data => {
      let base64Image = 'data:image/jpeg;base64,' + data;
      //this.currentPlace.photo.push(base64Image);
      this.locService.addPhoto(base64Image, this.currentPlace.timestamp);
    });
  }
}
