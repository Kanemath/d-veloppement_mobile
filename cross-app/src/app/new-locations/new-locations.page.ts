import { Component, OnInit } from '@angular/core';
import { Place } from '../model/place.model';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LocationsService } from '../services/locations.service';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';


@Component({
  selector: 'app-new-locations',
  templateUrl: './new-locations.page.html',
  styleUrls: ['./new-locations.page.scss'],
})
export class NewLocationsPage implements OnInit {

  constructor(private geolocation: Geolocation, private locatonService: LocationsService, private navCtrl: NavController) { }

  ngOnInit() {
  }

  onAddLocation(data: Place) {
    data.timestamp = new Date().getTime();
    data.photo = [];
    this.geolocation.getCurrentPosition().then(resp => {
      data.coordinates = {
        longitude: resp.coords.longitude,
        latitude: resp.coords.latitude
      };
      this.locatonService.addLoaction(data);
      this.navCtrl.back();
    });
  }
}
