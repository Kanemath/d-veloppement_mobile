import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthentificationService } from '../services/authentification.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
  menus = [
    {titile: 'Home', url: '/menu/home', icon: 'share'},
    {titile: 'Meteo', url: '/menu/meteo', icon: 'snow'},
    {titile: 'Gallery', url: '/menu/gallery', icon: 'school'},
    {titile: 'Locations', url: '/menu/locations', icon: 'sync'},
    {titile: 'Lagout', url: 'logout', icon: 'log-out'},
    {titile: 'Exit', url: 'Exit', icon: 'power'}
  ];
  constructor(private router: Router, private authService: AuthentificationService) { }

  ngOnInit() {
  }
  onMenuItem(m) {
    if (m.url === 'logout') {
      this.authService.logout();
      this.router.navigateByUrl('/login');
    } else if (m.url === 'Exit') {
      navigator['app'].exitApp();
    } else {
      this.router.navigateByUrl(m.url);
    }
  }
}
