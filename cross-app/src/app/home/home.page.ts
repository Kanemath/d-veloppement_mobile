import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public contact = {
    name: 'ESP',
    email: 'aladjikane14@gmail.com',
    tel: '774840831',
    logo: 'assets/imgs/logo-esp.png',
    loc: 'assets/imgs/location.png',
    log: 'assets/imgs/logos.jpg'
  };

  constructor() {}

}
