import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MeteoService {

  constructor(private httpClient: HttpClient) { }

  public getMeteoData(city: string) {
    return this.httpClient.get('http://api.openweathermap.org/data/2.5/forecast?q='+city+'&APPID=a4d36b4546a9f61e4311d5c6d9f91fb0');
  }
}
