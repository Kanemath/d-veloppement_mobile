import { Injectable } from '@angular/core';
import { Place } from '../model/place.model';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class LocationsService {
    private locations: Array<Place> = [];
    public currentLocation: Place;


  constructor(private storage: Storage) {
    this.locations.push({title: 'A'});
    this.locations.push({title: 'B'});
    this.locations.push({title: 'C'});
  }

  public addLoaction(place: Place) {
    this.locations.push(place);
    this.storage.set('locations', this.locations);
  }
  public getLocations() {
    return this.storage.get('locations').then(data => {
      this.locations = data != null ? data : [];
      return this.locations.slice();
    });
  }
  public updateLocation(locations) {
    this.storage.set('locations', locations);
  }
  public addPhoto(base64Image, timmestamp: number) {
    for (let index = 0; index < this.locations.length; index++) {
      if(this.locations[index].timestamp === timmestamp){
        this.locations[index].photo.push(base64Image);
        this.updateLocation(this.locations);
        break;
      }
    }
  }
}
